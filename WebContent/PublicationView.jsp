<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Publication</title>
</head>
 <%@ include file = "Header.jsp"%> 
<body>
<h1><a href="UpdatePublicationCitationsServlet"> </a></h1>
<table>
<tr>
    <td>${pi.id}</td>
    <td>${pi.publicationName}</td>
    <td>${pi.title}</td>
    <td>${pi.publicationDate}</td>
    <td>${pi.authors}</td>
    <td>${pi.citeCount}</td>
</tr>
</table>
</body>
</html>